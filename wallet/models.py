from django.db import models
from django.db.models import constraints


class Wallet(models.Model):
    wallet_id = models.CharField(max_length=16, unique=True, db_index=True)
    balance = models.IntegerField(default=1000)
    user = models.OneToOneField('auth.User', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.wallet_id} | {self.balance}"

    class Meta:
        constraints = [
            constraints.CheckConstraint(check=models.Q(balance__gte=0), name='min_value_0')
        ]


class WalletTransaction(models.Model):
    from_user = models.ForeignKey(Wallet, on_delete=models.SET_NULL, null=True, related_name='from_wallet')
    to_user = models.ForeignKey(Wallet, on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.from_user.wallet_id} -> {self.to_user.wallet_id}"




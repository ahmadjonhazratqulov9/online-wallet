from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Wallet, WalletTransaction

# class UserSerializers(serializers.Serializer):
#     id = serializers.IntegerField()
#     username = serializers.CharField(max_length=255)
#     is_staff = serializers.BooleanField()
#     is_active = serializers.BooleanField()


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'is_active',)

# class WalletSerializers(serializers.Serializer):
#     wallet_id = serializers.CharField(max_length=16)
#     balance = serializers.IntegerField()
#     user = UserSerializers()

class WalletSerializers(serializers.ModelSerializer):
    user = UserSerializers()
    class Meta:
        model = Wallet
        fields = ('id', 'wallet_id', 'balance', 'user')






class CreateTransactionSerializers(serializers.Serializer):
    from_wallet_id = serializers.CharField(max_length=16)
    to_wallet_id = serializers.CharField(max_length=16)
    amount = serializers.IntegerField()


class TransactionSerializers(serializers.ModelSerializer):
    class Meta:
        model = WalletTransaction
        fileds = '__all__'

